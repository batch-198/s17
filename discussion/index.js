// Functions
/*
	Syntax:
		function functionName(){
			code block(statement)
			}
*/

function printName(){
	console.log('My name is Anna');
};

// invoke / call the function
printName();
// result: My name is Anne

// functions need to be declared before they're used

// declaredFunction();
// results: error because it is not yet defined

// Function Declaration vs Expressions

declaredFunction();

function declaredFunction(){
	console.log("Hi I am from declaredFunction()");
};
declaredFunction();

// Function Expression
	// Anonymous function- function without a name

let variableFunction = function(){
	console.log('I am from variableFunction');
};

variableFunction();

let funcExpression = function funcName(){
	console.log('Hello from the other side');
};

funcExpression();

// You can reassign declared functions and function expressions to new anonymous function

declaredFunction = function(){
	console.log("Updated declaredFunction");
};

declaredFunction();

funcExpression = function(){
	console.log('Updated funcExpression');
};
funcExpression();

const constantFunction = function(){
	console.log('Initialize with const');
};

constantFunction();

// constantFunction = function(){
// 	console.log('Cannot be reassigned!');
// };

// constantFunction();


// Function Scoping
/*
	Javascript Variables has 3 types of scope:
	1. local/block scope
	2. global scope
	3. function scope

*/

{
	let localVar = "Armando Perez";
}

let globalVar = "Mr. Worldwide";

console.log(globalVar);
// console.log(localVar); result: error

function showNames(){
	// function scoped variable
	var functionVar = 'Joe';
	const functionConst = 'Nick';
	let functionLet = 'Kevin';

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
};

showNames();

/*console.log(functionVar);
console.log(functionConst);
console.log(functionLet);
Result: Error */

// Nested Function

function myNewFunction(){
	let name = 'Yor';

	function nestedFunction(){
		let nestedName = "Brando";
		console.log(nestedName);
	};
	nestedFunction();
};
myNewFunction();

// Function and Global Scoped Variable

// Global Scoped variable
let globalName = 'Alan';

function myNewFunction2(){
	let nameInside = "Marco";
	console.log(globalName);
}
myNewFunction2();
// Result: console.log(nameInside); Error


// alert('Hello World');

function showSampleAlert(){
	alert('Hello User!');
};
// showSampleAlert();

console.log("I will only log in the console when the alert is dismissed.");

// prompt()
/*
	Syntax:
		prompt("<dialog>");
*/

// let samplePrompt = prompt('Enter your name:');
// console.log('Hello ' + samplePrompt);

// let sampleNullPrompt = prompt("Don't enter anything");
// console.log(sampleNullPrompt);
// if prompt is cancelled, result will be: null
// if there is no input in the prompt, the result will be empty string

function printWelcomeMessage(){
	let firstName = prompt('Enter your first name:');
	let lastName = prompt('Enter your last name:');

	console.log('Hello, ' + firstName + " " + lastName + "!");
	console.log("Welcome to my page");
};

printWelcomeMessage();

// Function Naming Conventions
function getCourses(){
	let courses = ['Science 101', 'Arithmetic 103', 'Grammar 105'];
	console.log(courses);
};

getCourses();

// Name your functions in small caps. Foll camelcase when name functions

function displayCarInfo(){
	console.log('Brand: Toyota');
	console.log('Type: Sedan');
	console.log('Price: 1,500,000');
};

displayCarInfo();
