/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function userDetails(){
		let fullName = prompt('Enter your full name here: ');
		let age = prompt('Enter your age here: ');
		let location = prompt('Enter your address here: ');

		console.log('Hi'+ ' ' + fullName);
		console.log(fullName + "'s" + " age is " + age);
		console.log(fullName + " is located at " + location);
	};

	userDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function faveArtist(){
		let artists = ['Taeyeon','Red Velvet','Avril Lavigne','IU','Kinokoteikoku'];
		console.log(artists);
	};

	faveArtist();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function faveMovies(){
		let tomatometer = "Tomatometer for ";
		let movie1 = "Before Sunrise";
		let movie2 = "Before Sunset";
		let movie3 = "Before Midnight";
		let movie4 = "Interstellar";
		let movie5 = "5 Centimeters per Second";

		let movie1Rate = '100%';
		let movie2Rate = '94%';
		let movie3Rate = '98%';
		let movie4Rate = '73%';
		let movie5Rate = '88%';

		console.log("1." + movie1);
		console.log(tomatometer + movie1 + ":" + movie1Rate);
		console.log("2." + movie2);
		console.log(tomatometer + movie2 + ":" + movie2Rate);
		console.log("3." + movie3);
		console.log(tomatometer + movie3 + ":" + movie3Rate);
		console.log("4." + movie4);
		console.log(tomatometer + movie4 + ":" + movie4Rate);
		console.log("5." + movie5);
		console.log(tomatometer + movie5 + ":" + movie5Rate);
	}

	faveMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/
alert("Hi! Please add the names of your friends.");
let friend1 = prompt("Enter your first friend's name:"); 
let friend2 = prompt("Enter your second friend's name:"); 
let friend3 = prompt("Enter your third friend's name:");

let printFriends = function printUsers(){
	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();
// console.log(friend1);
// console.log(friend2);
// console.log(friend3);